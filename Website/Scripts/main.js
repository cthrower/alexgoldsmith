﻿/* Boostrap tooltips */
$(function () {
    // Resize full-img divs to be full-screen 
    $(window).on('load resize', function () {
        var divHeight = $(window).outerHeight() - ($('header').outerHeight() + $('footer').outerHeight());
        $('.full-img').css('height', divHeight);
    });

    // scroll-to div
    $('.read-more, .smoothscroll').on('click', function (e) {
        e.preventDefault();
        var target = $(this).data('target');
        $('html, body').animate({
            scrollTop: $(target).offset().top - 50
        }, 1000);
    });

    // Set free trial cookie when clicking free trial button
    $('[data-freetrial=true]').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/Home/SetAdwordsLandingSession"
        });
        window.location = "/account/register";
    });

    // Track Google events based on data- attributes
    // data-eventtrack="true" will initiliase
    // data-eventdata takes category & action
    // Example <a href="#" data-eventtrack="true" data-eventcat="Buttons" data-eventaction="Test">Link</a>
    $('[data-eventtrack=true]').on('click', function () {
        var $this = $(this);
        var category = $this.data('eventcat');
        var action = $this.data('eventaction');
        ga('send', 'event', category, action);
    });

    // show popover on hover, instead of click.
    $('[data-toggle="popover"]').hover(function (e) {
        $(this).popover('show');
    }, function (e) {
        $(this).popover('hide');
    });

    // Prefill potential package on free trial form when free trial button is clicked
    // Based on data-pack attribute
    $('body').on('click', '[data-trial=true]', function (e) {
        var pack = $(this).data('pack');
        console.log(pack);
        $('#PotentialPackage').val(pack);
    });

    // Init Parallax
    if ($('.page-wrap').hasClass('stellar')) {
        $(window).on('load resize', function () {
            $.stellar();
        });
    }

    $('[data-toggle="tooltip"]').tooltip();
    $('.btn-ajax').on('click', function () {
        $(this).addClass('disabled').find('.ajax-loader').fadeIn();
    });

    // Add class to nav when scrolling down
    $(window).on('scroll load', function () {
        var sticky = $('.navbar-default'),
			scroll = $(window).scrollTop();
        if (scroll >= 200) {
            sticky.addClass('scrolled');
        } else {
            sticky.removeClass('scrolled');
        }
    });

    /* Boostrap popover */
    $("[data-toggle=popover]")
    .popover({
        placement: 'top',
        html: true
    })
    $(document).on("click", ".popover .close", function () {
        $(this).parents(".popover").popover('hide');
    });

    /* Character Counter */
    $(".character-counter").on('input',
        function counterCount(e) {
            var maxlength = $(this).attr('data-val-length-max');
            if (typeof maxlength === undefined) {
                maxlength = 250;
            }
            var count = $(this).val().length;
            var remainder = maxlength - count;
            var html = "<span class=\"counter\">Characters remaining:" + remainder + "</span>";
            var old = $(this).next();
            if ($(old).hasClass("counter"))
                $(old).replaceWith(html);
            else
                $(this).after(html);
        }).each(function () { $(this).triggerHandler('input') });

    $("[data-val-length-max]").each(function (index, element) {
        var length = parseInt($(this).attr("data-val-length-max"));
        $(this).prop("maxlength", length);
    });
    attachMacroableFieldsHandlers();
    $('.btn-macro').on('click', function (e) {
        e.preventDefault();
        var macro = $(this).data('macro');
        var focused = "." + $('#classHolder').val();
        $(focused).insertAtCaret(macro);
    });

    /* Chat pop-up */
    $(".chat-header").click(function () {
        popupExpandedByUser = true;
        if ($(this).hasClass("clicked")) {
            $(this).removeClass("clicked");
            $("#chat-movable").animate({ bottom: "-345px" });
            document.cookie = "chatClosed=true;";
        } else {
            $(this).addClass("clicked");
            $("#chat-movable").animate({ bottom: "-5px" });
        }
    });

    $('.chat-header').click(function () {
        $(this).closest('.chat-container').toggleClass('collapsed');
    });

    var timer_id;
    $(window).resize(function () {
        clearTimeout(timer_id);
        timer_id = setTimeout(function () {
            makeTableResponsive();
        }, 300);
    });
    makeTableResponsive();


});

function addEvent(obj, evt, fn) {
    if (obj.addEventListener) {
        obj.addEventListener(evt, fn, false);
    }
    else if (obj.attachEvent) {
        obj.attachEvent("on" + evt, fn);
    }
}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};


// Store email from PPC landing page
function ppcLandingPageCapture(email) {
    $.ajax({
        type: "POST",
        url: "/Home/LandingPageCapture/",
        data: {
            email: email
        },
        dataType: "html"
    });
}

// Exit Intent popup
function exitIntent(intent) {
    //addEvent(window, "load", function (e) {
    //    addEvent(document, "mouseout", function (e) {
    //        e = e ? e : window.event;

    //        // If this is an autocomplete element.
    //        if (e.target.tagName.toLowerCase() == "input")
    //            return;

    //        // Get the current viewport width.
    //        var vpWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

    //        // If the current mouse X position is within 50px of the right edge
    //        // of the viewport, return.
    //        if (e.clientX >= (vpWidth - 50))
    //            return;

    //        // If the current mouse Y position is not within 50px of the top
    //        // edge of the viewport, return.
    //        if (e.clientY >= 50)
    //            return;

    //        // Reliable, works on mouse exiting window and
    //        // user switching active program
    //        var from = e.relatedTarget || e.toElement;
    //        if (!from)
    //            if (intent == "trial") {
    //                showTrialExitIntent();
    //            } else {
    //                showNormalExitIntent();
    //            }
    //    }.bind(this));
    //});
    //$('body').on('click', 'a:not(".start-trial-btn"):not(".higher"):not(".lower"):not(".ignore-exit")', function (e) {
    //    var url = e.currentTarget.href;
    //    if (url.indexOf('/account/register' || '/call-answering') === -1) {
    //        if (intent == "trial") {
    //            if (document.cookie.indexOf("trialExitIntentShown") < 0) {
    //                e.preventDefault();
    //                showTrialExitIntent(url);
    //            }
    //        } else {
    //            if (document.cookie.indexOf("normalExitIntentShown") < 0) {
    //                e.preventDefault();
    //                showNormalExitIntent(url);
    //            }
    //        }
    //    }
    //});
}

function showTrialExitIntent(url) {
    // Check if we have cookie
    if (document.cookie.indexOf("trialExitIntentShown") < 0) {
        // Set cookie
        var expireDate = new Date();
        expireDate.setTime(expireDate.getTime() + (1 * 3600 * 1000));
        expireDate = expireDate.toGMTString();
        document.cookie = "trialExitIntentShown=true; expires=" + expireDate + "; path=/";

        // Show popup
        swal({
            title: "Wait!",
            text: "<p>Before you go...<br><br><strong>Why not try our 7-day FREE trial?</strong><br><br>No contract, no commitment, no credit card required.<br><br>You’ve nothing to lose by trying it out, and you might just discover you love it.</p>",
            confirmButtonColor: "#f38630",
            confirmButtonText: "Start FREE trial now",
            showCancelButton: true,
            cancelButtonText: "No thanks",
            html: true,
            animation: "slide-from-top",
            type: "info"
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: "/Home/SetAdwordsLandingSession"
                });
                window.location = "/account/register";
            } else {
                if (url != null) {
                    window.location = url;
                }
            }
        });
    }
}
function showNormalExitIntent(url) {
    // Check if we have cookie
    if (document.cookie.indexOf("normalExitIntentShown") < 0) {
        // Set cookie
        var expireDate = new Date();
        expireDate.setTime(expireDate.getTime() + (1 * 3600 * 1000));
        expireDate = expireDate.toGMTString();
        document.cookie = "normalExitIntentShown=true; expires=" + expireDate + "; path=/";

        // Show popup
        swal({
            title: "Wait!",
            text: "<p>Before you go...<br><br><strong>Is there something you’re unsure of?</strong><br><br>If you’d like to ask a question, why not speak to a member of our team?<br><br>Call us, for free, on <a href=\"tel:08000096555\">0800 009 6555</a> now or <a href=\"#\" data-toggle=\"modal\" data-target=\"#callbackRequest\">arrange a call back</a>.</p>",
            confirmButtonColor: "#f38630",
            confirmButtonText: "Okay, thanks.",
            showCancelButton: true,
            cancelButtonText: "No thanks",
            html: true,
            animation: "slide-from-top",
            type: "info"
        }, function (isConfirm) {
            if (!isConfirm) {
                if (url != null) {
                    window.location = url;
                }
            }
        });
    }
}

function openCallBack() {
    /* Call Back Request */
    if ($('call-back.container2').hasClass('active')) {
        $('call-back.container2').removeClass('active');
        $("#callback").animate({ bottom: "-100%" });
    } else {
        $('call-back.container2').addClass('active');
        $("#callback").animate({ bottom: "-5px" });
    }
    $(".call-back.container2").click(function () {
        $(this).removeClass("clicked");
        $("#callback").animate({ bottom: "-100%" });
    });
    $('.call-back.container2').toggleClass('collapsed');
}

function getQueryString() {
    var queryLoc = window.location.href.indexOf('?');
    var params = {};
    if (queryLoc > 0) {
        var queryString = window.location.href.slice(queryLoc + 1);
        queryString.split("&").forEach(function (e) {
            var param = e.split("=");
            params[param[0]] = param[1];
        });
    }
    return params;
}

var popupExpandedByUser = false;
function popupLiveChat() {
    if (!popupExpandedByUser) {
        popupExpandedByUser = true;
        $('.chat-header').addClass("clicked");
        $('.chat-header').closest('.chat-container').toggleClass('collapsed');
        $("#chat-movable").animate({ bottom: "-5px" });
    }
}

function setDates() {
    var option = document.getElementById('quickDateSelector').value;
    var dates = option.split('|');
    document.getElementById('startDate').value = dates[0];
    if (dates.length === 2) {
        document.getElementById('endDate').value = dates[1];
    }
    document.getElementById('dateFilterSubmit').click();
}

function makeTableResponsive() {
    var mq = window.matchMedia("(max-width: 750px)");
    if (mq.matches) {
        $('.table tr td.grid-cell:visible:not(.not-responsive)').each(function (index, td) {
            if ($(td).children('.grid-cell-header').length === 0) {
                var th = $(td).closest('table').find('th').eq($(td).index())[0];
                if (th.innerText) {
                    var span = document.createElement("span");
                    span.innerText = th.innerText + ": ";
                    $(span).addClass('grid-cell-header').prependTo(td);
                }
            }
        });
    }
    else
        $('.table tr td:visible .grid-cell-header').remove();
};

function attachMacroableFieldsHandlers() {
    $('.form-control').focus(function () {
        $('#classHolder').val($(this).data('input'));
    });
}
jQuery.fn.extend({
    insertAtCaret: function (myValue) {
        return this.each(function (i) {
            if (document.selection) {
                this.focus();
                sel = document.selection.createRange();
                sel.text = myValue;
                this.focus();
            }
            else if (this.selectionStart || this.selectionStart === '0') {
                var startPos = this.selectionStart;
                var endPos = this.selectionEnd;
                var scrollTop = this.scrollTop;
                this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
                this.focus();
                this.selectionStart = startPos + myValue.length;
                this.selectionEnd = startPos + myValue.length;
                this.scrollTop = scrollTop;
            } else {
                this.value += myValue;
                this.focus();
            }
        })
    }
});

function getCookie(name, type) {
    if (document.cookie.indexOf(name) < 0) {
        return null;
    }
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    
    if (parts.length == 2) {
        // Check if we need int. If not just return it
        if (type != "int")
            return parts.pop().split(";").shift();

        var int = parseInt(getCookie(name));
        // Check if int is a number
        if (isNaN(int))
            return 0

        return int
    }
    return null;
}
